$(document).ready(function() {
    var firststate = false;
    var secondstate = false;
    var thirdstate = false;
    var isCollapsed = false;
    var delayTime = 800;
    var transitionEvent = whichTransitionEvent();
    
    function whichTransitionEvent() {
        var t, el = document.createElement("fakeelement");
        var transitions = {
            "transition"      : "transitionend",
            "OTransition"     : "oTransitionEnd",
            "MozTransition"   : "transitionend",
            "WebkitTransition": "webkitTransitionEnd"
          }
        
        for (t in transitions) {
            if (el.style[t] !== undefined){
              return transitions[t];
            }
        }
    } 
    
    $("#analytics").click(function() {
        $(this).one(transitionEvent,
            function() {
                $(this).removeClass("animate");
            });
        if ($("#analytics").hasClass('animate')) {
            console.log('animated');
            return false;
        }
        else if (isCollapsed && !firststate) {
            firststate = true;
            secondstate = false;
            thirdstate = false;
            changeState(1);
            console.log("1 state on!");
            //1е состояние вкл
        }
        else if (isCollapsed && firststate) {
            collapseMenu();
            firststate = false;
            changeState(1);
            console.log("1 state off!");
            //1е состояние выкл
        }
        else if (!isCollapsed) {
            collapseMenu();
            $(this).addClass("animate");
            firststate = true;
            changeState(1);
            console.log("1 state on and collapsed!");
            //1е состояние вкл
        }
     });  
    $("#materials").click(function() { 
        $(this).one(transitionEvent,
            function(event) {
                $(this).removeClass("animate");
            });
        if ($("#materials").hasClass('animate')) {
            console.log('animated');
        return false;
        }
        else if (isCollapsed && !secondstate) {
            secondstate = true;
            firststate = false;
            thirdstate = false;
            changeState(2);
            console.log("2 state on!");
            //2е состояние вкл
        }
        else if (isCollapsed && secondstate) {
            collapseMenu();
            secondstate = false;
            changeState(2);    
            console.log("2 state off!");
            //2е состояние выкл
        }
        else if (!isCollapsed) {
            collapseMenu();
            $(this).addClass("animate");
            secondstate = true;
            changeState(2);
            console.log("2 state on and collapsed!");
            //2е состояние вкл
        }
     }); 
    $("#supercondactors").click(function() { 
        $(this).one(transitionEvent,
            function(event) {
                $(this).removeClass("animate");
            });
        if ($("#supercondactors").hasClass('animate')) {
            console.log('animated');
        return false;
        }
        else if (isCollapsed && !thirdstate) {
            thirdstate = true;
            secondstate = false;
            firststate = false;
            console.log("3 state on!");
            changeState(3);
            //3е состояние вкл
        }
        else if (isCollapsed && thirdstate) {
            collapseMenu();
            thirdstate = false;
            changeState(3);
            console.log("3 state off!");
            //3е состояние выкл
        }
        else if (!isCollapsed) {
            collapseMenu();
            $(this).addClass("animate");
            thirdstate = true;
            changeState(3);
            console.log("3 state on and collapsed!");
            //3е состояние вкл
        }
     }); 
    
    function collapseMenu() {
        isCollapsed = isCollapsed != true; 
        $("#analytics").toggleClass("col-md-1 col-md-4");
        $("#materials").toggleClass("col-md-1 col-md-4");
        $("#supercondactors").toggleClass("col-md-1 col-md-4");
        $(".caption").delay(150).toggle(200);
        $("#materialBlock").show();
        $("#analyticBlock").show();
        $("#materialText").hide();
        $("#analyticText").hide();
        console.log(delayTime);
    }
    
    function changeState(num) {
        setAnimSpeed(); //switch construction?
        if (num == 1) {
            $("#materialsInfo").hide(300);
            $("#supercondactorsInfo").hide(300);
            $("#analyticsInfo").delay(delayTime).toggle(250);
        }
        else if (num == 2 ){
            $("#analyticsInfo").hide(300);
            $("#supercondactorsInfo").hide(300); 
            $("#materialsInfo").delay(delayTime).toggle(250);
        }
        else if (num == 3) {
            $("#materialsInfo").hide(300);
            $("#analyticsInfo").hide(300); 
            $("#supercondactorsInfo").delay(delayTime).toggle(250);
        }
    }
    
    function setAnimSpeed() {
        if (isCollapsed == true) {
            delayTime = 750;
            console.log("fast");
            console.log(delayTime);
        } else {
            delayTime = 0;
            console.log("slow");
            console.log(delayTime);
        }
    }
}); 