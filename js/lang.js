var dictionary = {
        'langname': {
            'ru': 'In English',
            'en': 'На Русском'
        },
        'heading1': {
            'ru': 'Аналитическое<br>оборудование',
            'en': 'Analytical<br>equipment',
        },
        'heading1n': {
            'ru': 'Аналитическое оборудование',
            'en': 'Analytical equipment',
        },
        'heading2': {
            'ru': 'Материалы',
            'en': 'Materials',
        },
        'heading3': {
            'ru': 'Сверхпроводники',
            'en': 'Supercondactors',
        },
        'subheading1': {
            'ru': 'Детекторы',
            'en': 'Detectors',
        },
        'subheading2': {
            'ru': 'Рентгеновские и нейтронные<br>камеры',
            'en': 'X-ray and neutron chambers',
        },
        'subheading2n': {
            'ru': 'Рентгеновские и нейтронные камеры',
            'en': 'X-ray and neutron chambers',
        },
        'subheading3': {
            'ru': 'Системы и устройства позиционирования',
            'en': 'Positioning devices',
        },
        'subheading4': {
            'ru': '5-этинил-2-дезоксиуридин EDU',
            'en': '5-Ethynyl-2-deoxyuridine (EdU)',
        },
        'subheading5': {
            'ru': 'Фторид фуллерена',
            'en': 'Fullerene fluoride',
        },
        'subheading6': {
            'ru': 'Монокристаллические подложки (металл)',
            'en': 'Monocrystalline substrates (metal)',
        },
        'subheading7': {
            'ru': 'Борированные материалы',
            'en': 'Flexible Neutron Shielding Products',
        },
        'subheading8': {
            'ru': 'Высокоориентированный<br>пиролетический графит',
            'en': 'HOPG - highly oriented pyrolytic graphite',
        },
        'textblock1': {
            'ru': '<h5>Детекторы</h5><p>Рентгеновская дифракция (рентгенодифракционный анализ) — один из дифракционных методов исследования структуры вещества. В основе данного метода лежит явление дифракции рентгеновских лучей на трёхмерной кристаллической решётке. Рассеяние рентгеновских лучей кристаллами (или молекулами жидкостей и газов) в результате взаимодействия рентгеновских лучей с электронами вещества, при котором из начального пучка лучей возникают вторичные отклоненные пучки той же длины волны.<br><br>На явлении дифракции рентгеновских лучей основаны такие методы исследования, как рентгеноструктурный анализ и порошковая рентгеновская дифракция. В основе рентгеноструктурного анализа лежит явление дифракции рентгеновских лучей на трехмерной кристаллической решетке отдельного монокристалла. Метод позволяет определять атомную структуру вещества, включающую в себя пространственную группу элементарной ячейки, ее размеры и форму, а также определить группу симметрии кристалла. Порошковая рентгеновская дифракция — метод исследования структурных характеристик материала при помощи дифракции рентгеновских лучей на порошке или поликристаллическом образце исследуемого материала. Результатом исследования является зависимость интенсивности рассеянного излучения от угла рассеяния. Метод позволяет определять качественный и полуколичественный состав образца, параметры элементарной ячейки образца, текстуру материала, размеры кристаллитов (области когерентного рассеяния) поликристаллического образца.<br><br>ООО «Кор-Сервис» имеет опыт поставки самых различных систем и детекторов для рентгеновской дифракции (Agilent Atlas, Rayonix SX165).<br><br>При поставке сложного оборудования монтаж и пуско-наладка осуществляется исключительно под контролем и при участии экспертов компании-производителя оборудования.</p>',
            'en': '<h5>Detectors</h5><br>X-ray crystallography is a technique used for determining the atomic and molecular structure of a crystal, in which the crystalline atoms cause a beam of incident X-rays to diffract into many specific directions. By measuring the angles and intensities of these diffracted beams, a crystallographer can produce a three-dimensional picture of the density of electrons within the crystal. From this electron density, the mean positions of the atoms in the crystal can be determined, as well as their chemical bonds, their disorder and various other information.<br><br>Since many materials can form crystals—such as salts, metals, minerals, semiconductors, as well as various inorganic, organic and biological molecules—X-ray crystallography has been fundamental in the development of many scientific fields. In its first decades of use, this method determined the size of atoms, the lengths and types of chemical bonds, and the atomic-scale differences among various materials, especially minerals and alloys. The method also revealed the structure and function of many biological molecules, including vitamins, drugs, proteins and nucleic acids such as DNA. X-ray crystallography is still the chief method for characterizing the atomic structure of new materials and in discerning materials that appear similar by other experiments. X-ray crystal structures can also account for unusual electronic or elastic properties of a material, shed light on chemical interactions and processes, or serve as the basis for designing pharmaceuticals against diseases.',
        },
        'textblock2': {
            'ru': '<h5>Рентгеновские и нейтронные камеры</h5><p> <ul><li>8x 6 mm Neutron or X-ray camera (200mm long)</li><li>80x60 mm Neutron or X-ray camera (55mm thick)</li>                                        <li>100x65 mm Neutron or X-ray camera (58mm thick)</li><li>100x50 mm Neutron or X-ray camera (43mm thick)</li><li>100x100 mm Neutron or X-ray camera (105mm thick)</li><li>120x 90 mm Neutron or X-ray camera (75 mm thick)</li><li>150x120 mm Neutron or X-ray camera (100mm thick)</li><li>200x150 mm Neutron or X-ray camera (125mm thick)</li></ul></p>',
            'en': '<h5>X-ray and neutron chambers</h5><p> <ul><li>8x 6 mm Neutron or X-ray camera (200mm long)</li><li>80x60 mm Neutron or X-ray camera (55mm thick)</li>                                        <li>100x65 mm Neutron or X-ray camera (58mm thick)</li><li>100x50 mm Neutron or X-ray camera (43mm thick)</li><li>100x100 mm Neutron or X-ray camera (105mm thick)</li><li>120x 90 mm Neutron or X-ray camera (75 mm thick)</li><li>150x120 mm Neutron or X-ray camera (100mm thick)</li><li>200x150 mm Neutron or X-ray camera (125mm thick)</li></ul></p>',
        },
        'textblock3': {
            'ru': ' <h5>Системы и устройства позиционирования</h5><p>Мы готовы предоставить вам широкий список систем позиционирования. Помимо стандартных устройств мы принимаем заявки на проектирование уникальных образцов.<br><br>Системы перевода<br><br><b>Материалы</b><br><br>Регулируемое основание электродвигателя:Анодированный алюминий<br>Направляющая система:Сталь<br>Приводная система:Сталь/Бронза<br><br><b>Точность</b><br>Каждый продукт представлен в трех категориях точности:<br><img src="images/snap.png"><br><br><b>Оборудование</b><br>Все системы оборудованы механическими концевыми выключателями. Также включены масштабы и соединения для адаптации к вашей приводной системе.<br><br><b>Комплектующие</b><br>Мы предлагаем расширенный ассортимент комплектующих под индивидуальные требования.<br><br><b>Общая информация</b><br>Huber translation systems are designed to meet the highest fine positioning requirements.<br>Все системы собраны из высококачественных материалов, прошедших современную обработку. Их статически оптимизированная конструкция гарантирует высокую горизонтальную неподвижность при минимально возможном весе.<br><br><b>Возможные комбинации</b><br>The series 5100 units can easily be combined with systems of the same and/or other series.В зависимости от требований существует возможность адаптирования и совмещения систем перевода и вращения для создания многоосевых систем позиционирования. </p> ',
            'en': '<h5>Positioning devices</h5><br><b>Materials</b><br><br>Base/Slide:Aluminium anodised<br>Guide System:Steel<br>Drive System:Steel/Bronze<br><br><b>Precision</b><br>Each product comes in up to three precision configurations:<br><img src="images/snap.png"><br><br><b>Equipment</b><br>All systems are equipped with mechanical limit switches. Also included are scales and couplings to adapt to your drive system.<br><br><b>Accessories</b><br> We offer an extensive range of accessories for individual requirements.<br><br></p> ',
        },
        'textblock4': {
            'ru': '<h5>5-этинил-2-дезоксиуридин EDU </h5><p>Используется для выявления ядер, синтезирующих ДНК.Не предназначено для животного или человека, терапевтического или диагностического использования.<ul><li>Форма: Твердое вещество</li><li>Метод обнаружения: Флуоресцентный</li><li>Состояние: Комнатная температура</li><li>Молекулярная формула: C 11 H 12 N 2 O 5</li><li>Молекулярный вес: 252,23</li><li>Чистота,— 99,0%</li><li>Упаковка: 50 мг, 5 г.</li></ul></p>',
            'en': '<h5>5-Ethynyl-2´-deoxyuridine (EdU)</h5><p>5-Ethynyl-2´-deoxyuridine (EdU) is a thymidine analogue which is incorporated into the DNA of dividing cells and is used to assay DNA synthesis in cell culture. At high doses it can be cytotoxic.<ul><li>Form: Solid</li><li>Detection Method: Fluorescent</li><li>Condition: Room temperature</li><li>Molecular formula: C 11 H 12 N 2 O 5</li><li>Molecular weight: 252,23</li><li>Purity,— 99,0%</li><li>Package: 50 mg, 5 g..</li></ul></p>',
        },
        'textblock5': {
            'ru': '<h5>Фторид Фуллерена </h5><p>Порошок золотисто-желтого цвета.<br>Дисперсность 100 мкм.<br>Чистота основного компонента 95 + - 3%.</p> ',
            'en': '<h5>Fullerene fluoride</h5><p>Golden yellow powder.<br>Dispersion:100 mkm.<br>Purity of the main component 95 + - 3%.</p> ',
        },
        'textblock6': {
            'ru': '<h5>Борированные Материалы </h5><p><ul><li>Гибкий лист</li><li>Самозатухающий гибкий лист</li><li>Лист резины</li><li>Защитный рентгеновский лист для фартуков</li><li>Самозатухающий борированный полиэтилен</li><li>Чистый полиэтилен</li></ul></p> ',
            'en': '<h5>FLEXIBLE NEUTRON SHIELDING PRODUCTS</h5><p><ul><li>Flexible Boron Sheet</li><li>Self Extinguishing Flexible Boron Sheet</li><li>Lead Loaded Boron Flexible Sheet</li><li>HOPG - highly oriented pyrolytic graphite</li></ul></p> ',
        },
        'textblock7': {
            'ru': '<h5>Высокоориентированный пиролетический графит</h5><p>Высокоориентированный пиролитический графит (ВОПГ, highly oriented pyrolytic graphite) — высокоориентированная форма пиролитического графита с угловым распространением c-оси меньше чем 1 градус. ВОПГ используется в качестве эталона длины для калибровки сканеров сканирующего туннельного микроскопа и атомно-силового микроскопа.<br>Коммерческий ВОПГ обычно производится отжигом при 3300 K. ВОПГ ведёт себя как очень чистый металл. Хорошо отражает свет и является хорошим проводником электричества, но очень ломкий. ВОПГ использовался как подложка во многих научных экспериментах.ВОПГ является материалом для изготовления графена методом механического отслоения.<br>Возможна поставка ВОПГ согласно спецификации заказчика. </p> ',
            'en': '<h5>HOPG - highly oriented pyrolytic graphite</h5><br><b>Applications</b><ul><li>Scanning tunneling microscopy-callibration and substrates</li><li>Graphite Monochromators</li><li>X-Ray diffraction</li><li>Neutron scattering and diffraction</li></ul><b>Physical Characteristics (at 300K)</b><br><img src="images/table1.png"><br><i>1) Mosaic spread is the half maximum height peak width of the Cu-Ka</i> <br><b> Products</b><br>HOPG plates are produced as flat, singly-bent and double-bent shapes. The monochromators are classified according to mosaic spread.<br><img src="images/table2.png"><br><i>* other sizes or specific mosaic spread on request</i><br> <i>** Tolerance ± 0/-1 mm</i><br> Standard radii for singly-bent plates: 115, 225, 250, 510, 790, and 1.300 mm.<br><b>Standard size for bent monochromators (mm):</b><br><img src="images/table3.png"><br> Thickness for bent plates 2 + 0/-1 mm.<br> Other sizes and specifications on request',
        },
        'textblock8': {
            'ru': '<h5>Монокристаллические подложки (металл) </h5><p>Монокристаллы по природе своей анизотропны, так как в разных направлениях имеют различную упорядоченность в расположении атомов. Это приводит к тому, что и физико-химические свойства одного и того же кристалла вдоль различных кристаллографических направлений, а также на поверхностях, ограничивающих кристалл, существенно различаются. Поэтому, при использовании монокристаллических электродов необходимо точно знать ориентацию рабочей поверхности электрода относительно главных кристаллографических направлений кристалла. От производителя же монокристаллических электродов требуется обеспечить необходимую ориентацию рабочей поверхности с заданной точностью. Для этого необходимо сперва определить расположение главных кристаллографических направлений в исходной монокристаллической заготовке. Достигается это при помощи метода дифракции рентгеновских лучей. </p> ',
            'en': '<h5>Monocrystalline substrates (metal)</h5><p>Monocrystals are anisotropic by nature, since in different directions they have different ordering in atomic arrangement. This leads to the fact that the physicochemical properties of the same crystal along different crystallographic directions, as well as the surfaces bounding the crystal, are significantly different. Therefore, when using a monocrystal electrodes it is needed to know exactly the orientation of the electrode working surface with respect to the main crystallographic directions of the crystal. From the manufacturer of the monocrystal electrodes is required to provide the necessary orientation of the work surface with a given accuracy. To do this, first must be determined the location of the main crystallographic directions in the original monocrystal workpiece. This is achieved by the method of X-ray diffraction.</p>',
        },
        'textblock9': {
            'ru': ' <img src="images/supercondact.jpg"><p>Высокотемпературный сверхпроводник второго поколения.Основание  (RE)BCO, с вариациями ширины, толщины подложки, толщины серебряного и медного стабилизатора, а также использовании дополнительной изоляции. ВТСП 2 изготавливается с помощью автоматизированного, непрерывного процесса с метода осаждения тонких пленок, аналогичного тому , который используется в полупроводниковой промышленности, для нанесения сверхпроводящего материала на буферизованных металлических подложках.<br>Возможна большая длина и покрытие изоляцией.<br> Особенности:<br>- превосходная производительность в магнитном поле из-за наноразмерных потоковых центров пиннинга, созданных за счет использования редкоземельного элемента в материалах;<br>- толщина проводника менее 0,1 мм;<br>- использование медного стабилизатора, отсутствие острых углов и возможность спайки<br>- подложка из Hastelloy ®: лучшие механические свойства и более низкие потери переменного тока, более низкие ферромагнитные потери <br>- высокая механическая прочность<br>- высокая величина критического тока  Ic</p>',
            'en': ' <img src="images/supercondact.jpg"><p>High-temperature superconductor of the second generation.The base (RE) BCO, with width variations, substrate thickness, the thickness of the silver and copper stabilizer, as well as the use of extra insulation. HTS 2 manufactured by an automated, continuous process with a thin film deposition method similar to that used in the semiconductor industry, to deposit the superconducting material on buffered metal substrates.<br>More length and insulation cover is available.<br> Features:<br>- Excellent performance in the magnetic field due to the flow of nanoscale pinning centers, created through the use of a rare earth element in the materials;<br>- The thickness of the conductor is less than 0.1 mm;<br>- The use of a copper stabilizer, the absence of sharp corners and the possibility of adhesions<br>-  Substrate Hastelloy ®: better mechanical properties and lower AC losses, lower ferromagnetic loss<br>- High mechanical strength<br>- The high value of the critical current Ic</p>',
        },
};
var langs = ['ru', 'en'];
var current_lang_index = 0;
var current_lang = langs[current_lang_index];

window.change_lang = function() {
    current_lang_index = ++current_lang_index % 2;
    current_lang = langs[current_lang_index];
    translate();
}

function translate() {
    $("[data-translate]").each(function(){
        var key = $(this).data('translate');
        $(this).html(dictionary[key][current_lang] || "N/A");
    });
}

translate();