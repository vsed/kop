function showText(divid) {
    $('.infoTextBlock').each(function(index) {
        activateButton(divid);
        
        if ($(this).attr("id") == divid) {
            $(this).show(350);
        } else {
                   $(this).hide(200);
              }
    });
}
function activateButton(ndivid) {
    var x = 0;
    $('.toggleButton').removeClass('active1');    
    $('.toggleButton').removeClass('active2');
    var x = "n" + ndivid;
    if ($('#'+x).hasClass("orange")) {
        $('#'+x).addClass('active2');
    } else { 
        $('#'+x).addClass('active1'); 
    }  
}
function gotoNextLevel(num) {
    switch (num) {
        case 1:
            $("#analyticBlock").hide(300);
            $("#analyticText").show(500);
            break;
        case 2:
            $("#materialBlock").hide(300);
            $("#materialText").show(500);
            break;
            }
}